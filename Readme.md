Compilation & Launch

1. Install java jdk
2. [To Compile and run] javac RedditClone.java && java RedditClone
3. Launch http://localhost:8500 via postman or browser and see the available commands.

Known bugs:

1. /redditclone?vote=up&uniqueid=5 works but /redditclone?uniqueid=5&vote=up will not work due to the way parsing is done. Easy to fix though.
   Take note that there is NO Frontend at the moment.
