public class Node {
    private String topic;
    private int votes, id, uniqueId;

    public Node(String topic) {
        this.topic = topic;
        this.votes = 0;
    }

    public String getTopic() {
        return topic;
    }

    public int getVotes() {
        return votes;
    }

    public void upVote() {
        this.votes++;
    }

    public void downVote() {
        this.votes--;
    }

    public int getId() {
        return this.id;
    }

    public void setUniqueId(int id) {
        this.id = id;
        this.uniqueId = id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String toString() {
        return "\n{" + "\"Topic\": \"" + topic + "\"," + "\"Votes\": \"" + votes + "\"," + "\"Id\": \"" + id + "\","
                + "\"UniqueId\": \"" + uniqueId + "\"" + "}";
    }
}
/*
 * Sample json { "Topic": "Hello", "Id": "123", "UniqueId": "123" }
 */