import java.util.ArrayList;

public class MaxHeap {
    private ArrayList<Node> nodes; // This is the max heap binary tree arraylist
    private ArrayList<Node> allNodes; // This arraylist is to maintain a unique ID or hash of the nodes.
    private boolean top20Changed;
    private ArrayList<Node> maxArrayList;

    public MaxHeap() {
        nodes = new ArrayList<Node>();
        allNodes = new ArrayList<Node>();
        top20Changed = true;
        maxArrayList = new ArrayList<Node>();
    }

    public Node getMax() {
        if (nodes.size() == 0) {
            System.out.println("Unable to get Max, we do not have any elements inside.");
            return null;
        } else
            return nodes.get(0);
    }

    // Get the parent arraylist id
    private int getParentId(int id) {
        return ((id - 1) / 2);
    }

    // Get the left child arraylist id
    private int getLeftChildId(int id) {
        return ((id * 2) + 1);
    }

    // Get the right child arraylist id
    private int getRightChildId(int id) {
        return ((id * 2) + 2);
    }

    // We are inserting a new node
    public void insertNode(Node n) {
        n.setUniqueId(nodes.size());
        nodes.add(n);
        allNodes.add(n);
        moveUp(nodes.size() - 1);
    }

    private Node removeMax() {
        if (nodes.size() == 0) {
            return null;
        } else {
            Node currMaxNode = nodes.get(0);
            Node maxNode = nodes.get(nodes.size() - 1);

            nodes.set(0, maxNode);
            nodes.remove(nodes.size() - 1);
            if (nodes.size() > 0) {
                moveDown(0, true);
            }
            return currMaxNode;
        }
    }

    // Helper Function to return the final heap tree.
    public ArrayList<Node> printArrayList() {
        return nodes;
    }

    // Helper Function to return the original ID of all elements.
    public ArrayList<Node> printUniqueArrayList() {
        return allNodes;
    }

    public void upVoted(int uniqueId) {
        Node tmp = allNodes.get(uniqueId);
        tmp.upVote();
        moveUp(tmp.getId());
    }

    public void downVoted(int uniqueId) {
        Node tmp = allNodes.get(uniqueId);
        tmp.downVote();
        moveDown(tmp.getId(), false);
    }

    // Checks with parent and swaps if neccessary.
    private void moveUp(int childId) {
        if (childId != 0) { // Implies it isn't the MAXIMUM cause a root cannot have a parent.
            int parentId = getParentId(childId);
            Node parentNode = nodes.get(parentId);
            Node childNode = nodes.get(childId); // Current Node

            // We do a swap if the parent is smaller.
            if (parentNode.getVotes() < childNode.getVotes()) {
                parentNode.setId(childId);
                childNode.setId(parentId);

                nodes.set(parentId, childNode); // We do the swap here
                nodes.set(childId, parentNode);

                top20Changed = true;
                moveUp(parentId); // Recurse the same node to check if it is higher than all parents.
            }
        }
    }

    // Checks with Children and swap if neccessary.
    private void moveDown(int parentId, boolean removeMax) {
        int leftChildId = getLeftChildId(parentId);
        int rightChildId = getRightChildId(parentId);
        int maxChildId;
        Node toBeChild, toBeParent;

        if (rightChildId <= nodes.size() - 1) { // Checks if there is a left & right node
            if (nodes.get(leftChildId).getVotes() >= nodes.get(rightChildId).getVotes())
                maxChildId = leftChildId;
            else
                maxChildId = rightChildId;
        } else if (leftChildId <= nodes.size() - 1) // Only left node.
            maxChildId = leftChildId;
        else // Its the lowest child for it's 'section', thus no more children!
            return;

        if (nodes.get(parentId).getVotes() < nodes.get(maxChildId).getVotes()) {
            toBeParent = nodes.get(maxChildId);
            toBeChild = nodes.get(parentId);

            if (!removeMax) {
                toBeParent.setId(parentId);
                toBeChild.setId(maxChildId);
            }

            nodes.set(parentId, toBeParent); // We do the swap here
            nodes.set(maxChildId, toBeChild);

            top20Changed = true;
            moveDown(maxChildId, removeMax);
        }
    }

    public ArrayList<Node> getTopX(int no) {
        if (top20Changed == false) {
            return maxArrayList;
        }
        maxArrayList.clear();
        ArrayList<Node> origArrayList = new ArrayList<Node>(nodes);
        for (int i = 0; i < no; i++) {
            Node maxNode = removeMax();
            if (maxNode == null) {
                nodes = origArrayList;
                top20Changed = false;
                return maxArrayList;
            } else {
                maxArrayList.add(maxNode);
            }
        }

        nodes = origArrayList;
        top20Changed = false;
        return maxArrayList; // No elements in the list!
    }
}