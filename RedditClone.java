import com.sun.net.httpserver.*;
import java.io.*;
import java.net.*;

public class RedditClone {
	static MaxHeap mh = new MaxHeap();

	public static void main(String[] args) throws IOException {
		initalizeNodes(mh);
		System.out.println("-----------------");
		HttpServer server = HttpServer.create(new InetSocketAddress(8500), 0);
		HttpContext context = server.createContext("/redditclone");
		context.setHandler(RedditClone::handleRequest);
		server.start();
	}

	private static void handleRequest(HttpExchange exchange) throws IOException {
		URI requestURI = exchange.getRequestURI();
		String query = requestURI.getQuery();
		String response;
		if (query == null) {
			response = "Please use the following APIs to properly access the web."
					+ "\n1: /redditclone?addtopic=This is a test" + "\n2: /redditclone?top20"
					+ "\n3: /redditclone?vote=up&uniqueid=5" + "\n4: /redditclone?vote=down&uniqueid=5"
					+ "\n5: /redditclone?listall";
		} else if (query.contains("addtopic=")) {
			Node n = new Node(query.split("addtopic=")[1]);
			mh.insertNode(n);
			response = "Added! Please visit <a href='/redditclone?listall'listall</a> to view your list."; // WIP
		} else if (query.equals("top20")) {
			response = mh.getTopX(20).toString();
		} else if (query.contains("vote=up") && query.contains("&uniqueid=")) {
			int id = Integer.valueOf(query.split("uniqueid=")[1]);
			mh.upVoted(id);
			response = mh.toString();
		} else if (query.contains("vote=down") && query.contains("&uniqueid=")) {
			int id = Integer.valueOf(query.split("uniqueid=")[1]);
			mh.downVoted(id);
			response = mh.toString();
		} else if (query.equals("listall")) { // Not in the requirement but good for debugging.
			response = mh.printUniqueArrayList().toString();
		} else {
			response = "INVALID";
			exchange.sendResponseHeaders(400, response.getBytes().length);// response code and length
			OutputStream os = exchange.getResponseBody();
			os.write(response.getBytes());
			os.close();
			return;
		}
		exchange.sendResponseHeaders(200, response.getBytes().length);// response code and length
		OutputStream os = exchange.getResponseBody();
		os.write(response.getBytes());
		os.close();
	}

	private static void initalizeNodes(MaxHeap mh) {

		for (int i = 0; i < 11; i++) {
			Node x = new Node("N" + i);
			for (int j = 0; j < i; j++) {
				x.upVote();
			}
			mh.insertNode(x);
		}

		/*
		 * Node n = new Node("N"); Node n1 = new Node("N1"); Node n2 = new Node("N2");
		 * Node n3 = new Node("N3"); n.upVote(); n.upVote(); n.upVote(); n.upVote();
		 * n.upVote();
		 * 
		 * n1.upVote();
		 * 
		 * n2.downVote(); n3.upVote(); n3.upVote(); n3.upVote(); n3.upVote();
		 * 
		 * mh.insertNode(n); mh.insertNode(n1); mh.insertNode(n2); mh.insertNode(n3);
		 * 
		 * n3.downVote(); mh.downVoted(n3.getId()); n3.downVote();
		 * mh.downVoted(n3.getId()); n3.downVote(); mh.downVoted(n3.getId());
		 * n3.downVote(); mh.downVoted(n3.getId());
		 */
	}
}